import asyncio
import copy
import json
import logging
import os
import random
import sys
from typing import Set, Optional, List, Tuple, Collection, Dict

from redis import StrictRedis
from traze.bot import Action, BotBase
from traze.client import World, Game, Grid

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger("SelfLearningBot")

ENV_VARIABLE_CONFIG_PATH = 'CONFIG_PATH'
if ENV_VARIABLE_CONFIG_PATH in os.environ:
    config_path = os.environ.get(ENV_VARIABLE_CONFIG_PATH)
else:
    config_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               '../../resources/config.json')

with open(config_path, 'r', encoding='utf-8') as config_file:
    config = json.loads(config_file.read())


def to_json(o: Collection):
    return json.dumps(o)


class ActionHistoryItem:

    def __init__(self, state: Collection, taken_action: Action, possible_actions: Set[Action]):
        self._state = state
        self._taken_action = taken_action
        self._possible_actions = possible_actions

    @property
    def state(self) -> Collection:
        return self._state

    @property
    def taken_action(self) -> Action:
        return self._taken_action

    @property
    def possible_actions(self) -> Set[Action]:
        return self._possible_actions


class ActionHistory:

    def __init__(self):
        self._items = []

    def reset(self):
        self._items = []

    def add(self, action_item: ActionHistoryItem):
        logger.debug('Adding new HistoryItem to History')
        if len(self._items) >= config['ACTION_HISTORY_LENGTH']:
            del self._items[0]
        self._items.append(action_item)

    @property
    def items(self) -> List[ActionHistoryItem]:
        return self._items


class SelfLearningBot(BotBase):

    def __init__(self, game: 'Game'):
        super().__init__(game)
        self._redis: StrictRedis = StrictRedis(host=config['REDIS_HOST'],
                                               port=int(config['REDIS_PORT']),
                                               db=int(config['REDIS_DB_NUMBER']))
        self._action_history: ActionHistory = ActionHistory()

        def on_ticker(payload: object):
            copied_action_history = copy.deepcopy(self._action_history)
            self._action_history.reset()

            # Well, I died
            if payload['casualty'] == self._id:
                asyncio.run(self.save_experiences(copied_action_history, config['MIN_SCORE']))
            # I killed someone, yeah!
            elif payload['fragger'] == self._id:
                asyncio.run(self.save_experiences(copied_action_history, config['MAX_SCORE']))

        self.adapter.on_ticker(self.game.name, on_ticker)

    @staticmethod
    def get_tile_relativ_from(x_diff: int, y_diff: int, player_position: Tuple[int], grid: Grid) -> \
            Optional[int]:
        """

        :param x_diff:
        :param y_diff:
        :param player_position:
        :param grid:
        :return: None if wanted tile is not on grid
        """
        new_x = player_position[0] + x_diff
        new_y = player_position[1] + y_diff

        if new_x < 0 or new_y < 0 or new_x > (grid.height - 1) or new_y > (grid.width - 1):
            return False
        return bool(grid.tiles[new_x][new_y])

    @staticmethod
    def aggregate_tiles(tiles: List[int]) -> bool:
        number_of_empty_tiles = 0
        for tile in tiles:
            if not tile:
                number_of_empty_tiles += 1

        return number_of_empty_tiles >= (len(tiles))

    @staticmethod
    def current_state_to_dict(current_player_position: Tuple[int], current_game_state: Game) -> \
            List[int]:
        result = []

        # Direct neighbors of player
        result.append(SelfLearningBot.get_tile_relativ_from(1, 1, current_player_position,
                                                            current_game_state.grid))
        result.append(SelfLearningBot.get_tile_relativ_from(1, 0, current_player_position,
                                                            current_game_state.grid))
        result.append(SelfLearningBot.get_tile_relativ_from(1, -1, current_player_position,
                                                            current_game_state.grid))
        result.append(SelfLearningBot.get_tile_relativ_from(0, 1, current_player_position,
                                                            current_game_state.grid))
        result.append(SelfLearningBot.get_tile_relativ_from(0, -1, current_player_position,
                                                            current_game_state.grid))
        result.append(SelfLearningBot.get_tile_relativ_from(-1, 1, current_player_position,
                                                            current_game_state.grid))
        result.append(SelfLearningBot.get_tile_relativ_from(-1, 0, current_player_position,
                                                            current_game_state.grid))
        result.append(SelfLearningBot.get_tile_relativ_from(-1, -1, current_player_position,
                                                            current_game_state.grid))

        # Aggregated Tiles
        result.append(SelfLearningBot.aggregate_tiles([SelfLearningBot.get_tile_relativ_from(-2, 1,
                                                                                             current_player_position,
                                                                                             current_game_state.grid),
                                                       SelfLearningBot.get_tile_relativ_from(-2, 0,
                                                                                             current_player_position,
                                                                                             current_game_state.grid),
                                                       SelfLearningBot.get_tile_relativ_from(-2, -1,
                                                                                             current_player_position,
                                                                                             current_game_state.grid)]))
        result.append(SelfLearningBot.aggregate_tiles([SelfLearningBot.get_tile_relativ_from(2, 1,
                                                                                             current_player_position,
                                                                                             current_game_state.grid),
                                                       SelfLearningBot.get_tile_relativ_from(2, 0,
                                                                                             current_player_position,
                                                                                             current_game_state.grid),
                                                       SelfLearningBot.get_tile_relativ_from(2, -1,
                                                                                             current_player_position,
                                                                                             current_game_state.grid)]))
        result.append(SelfLearningBot.aggregate_tiles([SelfLearningBot.get_tile_relativ_from(1, -2,
                                                                                             current_player_position,
                                                                                             current_game_state.grid),
                                                       SelfLearningBot.get_tile_relativ_from(0, -2,
                                                                                             current_player_position,
                                                                                             current_game_state.grid),
                                                       SelfLearningBot.get_tile_relativ_from(-1, -2,
                                                                                             current_player_position,
                                                                                             current_game_state.grid)]))
        result.append(SelfLearningBot.aggregate_tiles([SelfLearningBot.get_tile_relativ_from(1, 2,
                                                                                             current_player_position,
                                                                                             current_game_state.grid),
                                                       SelfLearningBot.get_tile_relativ_from(0, 2,
                                                                                             current_player_position,
                                                                                             current_game_state.grid),
                                                       SelfLearningBot.get_tile_relativ_from(-1, 2,
                                                                                             current_player_position,
                                                                                             current_game_state.grid)]))

        # Add corners
        result.append(SelfLearningBot.get_tile_relativ_from(2, 2, current_player_position,
                                                            current_game_state.grid))
        result.append(SelfLearningBot.get_tile_relativ_from(2, -2, current_player_position,
                                                            current_game_state.grid))
        result.append(SelfLearningBot.get_tile_relativ_from(-2, 2, current_player_position,
                                                            current_game_state.grid))
        result.append(SelfLearningBot.get_tile_relativ_from(-2, -2, current_player_position,
                                                            current_game_state.grid))

        return result

    @staticmethod
    def select_action(experiences: dict, possible_actions: Set[Action]) -> Action:
        if random.random() >= float(config['PROBABILITY_OF_RANDOM_ACTION']) and len(
                experiences.items()):
            highest_score = None
            best_action = None
            for experienced_action, experienced_value in experiences.items():
                if highest_score is not None:
                    if experienced_value > highest_score:
                        highest_score = experienced_value
                        best_action = experienced_action
                else:
                    highest_score = experienced_value
                    best_action = experienced_action

            return Action.from_name(best_action)
        else:
            return random.choice(tuple(possible_actions))

    def get_score_for_action_in_state(self, action: Action, state: Collection) -> Optional[Dict]:
        experiences_in_redis = self.redis_get(to_json(state), default="{}", transform_json=True)
        if action.name in experiences_in_redis.keys():
            return experiences_in_redis[action.name]
        else:
            return None

    async def save_experiences(self, action_history: ActionHistory, reached_max_score: int):
        logger.info('Saving my experiences with a score of {}'.format(reached_max_score))
        number_of_history_items = len(action_history.items)

        for idx, action_history_item in enumerate(action_history.items):
            score_for_number_of_steps = number_of_history_items * int(config['SCORE_PER_STEP'])
            epsilon = ((idx - 1) / number_of_history_items)
            state = action_history_item.state
            old_score = self.get_score_for_action_in_state(action_history_item.taken_action,
                                                           action_history_item.state)
            if old_score is None:
                old_score = 0

            highest_score_from_next_step = 0
            for possible_action in action_history_item.possible_actions:
                score_with_action = self.get_score_for_action_in_state(possible_action,
                                                                       action_history_item.state)
                if score_with_action is None:
                    score_with_action = 0
                if score_with_action > highest_score_from_next_step:
                    highest_score_from_next_step = score_with_action

            current_score = (1 - int(config['LEARNING_RATE'])) * old_score + (
                    score_for_number_of_steps + reached_max_score + epsilon * highest_score_from_next_step)

            state_json = to_json(state)
            experiences_in_redis = self.redis_get(state_json, default=None, transform_json=True)
            if experiences_in_redis:
                experiences_in_redis[action_history_item.taken_action.name] = current_score
            else:
                experiences_in_redis = {
                    action_history_item.taken_action.name: current_score
                }

            self.redis_write(state_json, to_json(experiences_in_redis))

    def redis_get(self, key, default=None, transform_json=False):
        if self._redis.exists(key):
            result = self._redis.get(key)
        else:
            result = default

        if transform_json:
            if result:
                return json.loads(result)
            else:
                return result
        else:
            return result

    def redis_write(self, key, value):
        self._redis.set(key, value)

    def next_action(self, actions: Set[Action]) -> Optional[Action]:
        if not actions:
            return None
        current_state = self.current_state_to_dict((self.x, self.y), self.game)
        experiences = self.redis_get(to_json(current_state), default="{}", transform_json=True)

        next_action = self.select_action(experiences, actions)

        self._action_history.add(ActionHistoryItem(current_state, next_action, actions))

        return next_action


if __name__ == "__main__":
    SelfLearningBot(World().games[0]).play(int(config['NUMBER_OF_PLAYS']),
                                           suppress_server_timeout=True)
